## In general

- I was only able to skim through the papers and filter them, so I only have very superficial impression on them so far.
- Most research concerning credit scoring mostly focuses on using customer data from application forms or financial ratios of firms. Papers doing credit scoring based on transactional data are relatively sparse. The same applies for the corresponding datasets.
- Almost all of the research papers I found so far are in the B2C scenarios, I only found 1 real B2B case.
- I haven't found any credit scoring papers comparing model predictive performances between using vs. not using transactional data. 	

## B2B 

|Items | Comments |
| ---  | -------- |
Title|			Predicting Default Probability Using Delinquency; The Case of French Small Businesses |
Type|			Article in SSRN |
Date| 			Feb 2014 |
Institute|		University of Cergy-Pontoise |
Data| 			French MSMEs from GE's factoring database| Historical payment data from GE + Commercial litigation and debt to French government from Coface Service |
Application|	Predict 6-months default rate |


## B2C 


|Items | Comments |
| ---  | -------- |
Title| 			Consumer credit-risk models via machine learning algorithm
Type|			Article in Journal of Banking & Finance
Date| 			March 2010
Institute|		MIT Sloan School of Management and Laboratory for Financial Engineering
Data| 			Transaction level, credit bureau, account-balance data of individual consumers from the bank's consumerbase
Application|	90-days-or-more credit card delinquency rate prediction
---

|Items | Comments |
| ---  | -------- |
Title| 			Big Data and Credit Unions| Machine Learning in Member Transactions
Type|			Research reports
Date| 			Jun 2013
Institute|		Filene Research Institute 
Data| 			Transactional data from five participating credit unions.
Application|	Credit scoring, cluster switches prediction, Customer life cycle analysis
---

|Items | Comments |
| ---  | -------- |
Title| 			A Window of Opportunity| Assessing Behavioural Scoring
Type|			Articles uin International Journal of Expert Systems with Applications
Date| 			2013	
Institute|		School of Computing, Dublin Institute of Technology, Irish Credit Bureau
Data| 			Data from Irish Credit Bureau (application data + repayment behaviour for 7 years)			
Application|	Behavioural Scoring, Predict default within a given time window.
---

|Items | Comments |
| ---  | -------- |
Title| 			Checking account activity and credit default risk of enterprises| An application of statistical learning methods.
Type|			Arxiv paper.
Date| 			July 2017
Institute|		Ecole Polytechnique, Societe generale
Data| 			Financial ratios + Corporate transactions 
Application|	Prediction of corporate default within 1 -year using corporate financial ratios + corporate account activities
---

|Items | Comments |
| ---  | -------- |
Title|			CoMoVi| A Framework for Data Transformation in Credit Behavioral Scoring Applications Using Model Driven Architecture			
Type|			Article in SEKE
Date| 			2014
Institute|		Federal University of Pernambuco (UFPE), Federal University of Sao Francisco Valley (Univasf), Brazil
Data| 			Public dataset PKDD'99 Discovery Challenge
Application|	Credit Behavioral Scoring using application + transaction data
---

|Items | Comments |
| ---  | -------- |
Title|			Forcasting and Stress Testing Credit Card Default using Dynamic Models 
Type|					
Date| 			Nov 2009			
Institute|		Credit Research centre Uni. of Edinburgh Business School
Data| 			application data + behavioural data about credit card holders + macroeconomic conditions
Application|	Discrete survival models for credit card default
---

## White papers


|Items | Comments |
| ---  | -------- |
Title|			Understand Customer Tempo to Improve Marketing Results |
Type|			White Paper |
Date| 			2008 |
Institute|		First Data |
Data| 			|
Application|	Customer Tempo Analysis based on transaction data, an analysis over sectors cross-section, more about customer monitoring less about credit risk prediction |
---

|Items | Comments |
| ---  | -------- |
Title| 			Is B2B Purchase Behavior The Ultimate Credit Barometer?
Type|			White Paper
Date| 			2012
Institute|		Cortera
Data| 			
Application|	purschasing data focused B2B credit solution
---

## Maybe Related


|Items | Comments |
| ---  | -------- |
|Title| 			Combining Accounting Data and a Structural Model for Predicting Credit Ratings| Empirical Evidence from |European Listed Firms.|
|Type|						
|Date| 			May 2014|
|Institute|		
|Data| 			
|Application|	Credit Rating prediction with financial ratio + market variables.|
---

## Survey Papers|


|Items | Comments |
| ---  | -------- |
|Title| 			An Insight into the experimental design for credit risk and corporate bankruptcy prediction systems.|
|Type|			Article in Journal of Intelligent Information Systems (Survey paper)|
|Date| 			Feb 2014|
|Institute|		Universidad	Autónoma de	Ciudad Juárez, Universitat Jaume I|
|Data| 			|
|Summary|		Talks about the common datasets, techniques and evaluation metrics.|
